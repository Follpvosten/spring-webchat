package xyz.karpador.webchat.data;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Long> {
    Optional<User> findByActiveTrueAndSession(String session);
    Optional<User> findByActiveTrueAndStompSession(String stompSession);
    Optional<User> findByActiveTrueAndName(String name);

    List<User> findByActiveTrueAndStompSessionNotNull();
}
