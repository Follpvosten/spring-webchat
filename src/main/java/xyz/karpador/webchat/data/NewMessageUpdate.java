package xyz.karpador.webchat.data;

import lombok.Getter;

public class NewMessageUpdate extends Update {
	@Getter
	private final Message message;

	public NewMessageUpdate(Message message) {
		super(UpdateKind.NewMessage);
		this.message = message;
	}
}
