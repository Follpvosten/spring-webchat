package xyz.karpador.webchat.data;

import java.sql.Timestamp;
import java.util.Optional;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

@Data
@Entity
public class Message {

	private @Id
	@GeneratedValue
	Long id;
	private String content;
	private Timestamp sentTimestamp;
	private Timestamp editedTimestamp;
	private String color;

	@JsonIgnore
	@ManyToOne
	private User user;

	public Message() {}

	public Message(String content, Timestamp sentTimestamp, String color, User user) {
		this.content = content;
		this.sentTimestamp = sentTimestamp;
		this.user = user;
		this.color = color;
	}

	public Optional<User> getUser() {
		return user == null ? Optional.empty() : Optional.of(user);
	}

	public String getUserName() {
		return getUser().map(User::getName).orElse("Spring Chat");
	}
}
