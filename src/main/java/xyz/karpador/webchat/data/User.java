package xyz.karpador.webchat.data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

@Data
@Entity
public class User {

	@Id
	@GeneratedValue
	private Long id;
	private String name;
	private boolean active;

	@JsonIgnore
	private String session;
	@JsonIgnore
	private String stompSession;

	public User() {}

	public User(String name, boolean active, String session, String stompSession) {
		this.name = name;
		this.session = session;
		this.active = active;
		this.stompSession = stompSession;
	}
}
