package xyz.karpador.webchat.data;

import lombok.Getter;

public abstract class Error {
	public enum ErrorKind {
		SessionError,
		OtherError
	}

	@Getter
	private final ErrorKind errorKind;

	protected Error(ErrorKind errorKind) {
		this.errorKind = errorKind;
	}
}
