package xyz.karpador.webchat.data;

import lombok.Getter;

public class GeneralError extends Error {
	@Getter
	private final String message;

	public GeneralError(String message) {
		super(ErrorKind.OtherError);
		this.message = message;
	}
}
