package xyz.karpador.webchat.data;

import lombok.Getter;

public class UserJoinedUpdate extends Update {
	@Getter
	private final String name;

	public UserJoinedUpdate(String name) {
		super(UpdateKind.UserJoined);
		this.name = name;
	}
}
