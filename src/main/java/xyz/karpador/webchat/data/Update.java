package xyz.karpador.webchat.data;

import lombok.Getter;

public abstract class Update {
	public enum UpdateKind {
		NewMessage,
		EditedMessage,
		UserJoined,
		UserLeft
	}
	@Getter
	private final UpdateKind updateKind;

	protected Update(UpdateKind updateKind) {
		this.updateKind = updateKind;
	}
}
