package xyz.karpador.webchat.data;

import lombok.Getter;

import java.sql.Timestamp;

public class EditedMessageUpdate extends Update {
	@Getter
	private final Long messageId;
	@Getter
	private final String newContent;
	@Getter
	private final Timestamp editedTime;

	public EditedMessageUpdate(Long messageId, String newContent, Timestamp editedTime) {
		super(UpdateKind.EditedMessage);
		this.messageId = messageId;
		this.newContent = newContent;
		this.editedTime = editedTime;
	}
}
