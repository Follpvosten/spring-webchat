package xyz.karpador.webchat.data;

import lombok.Getter;

public class UserLeftUpdate extends Update {
	@Getter
	private final String name;

	public UserLeftUpdate(String name) {
		super(UpdateKind.UserLeft);
		this.name = name;
	}
}
