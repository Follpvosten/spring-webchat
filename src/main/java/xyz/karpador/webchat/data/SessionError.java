package xyz.karpador.webchat.data;

import lombok.Getter;

public class SessionError extends Error {
	@Getter
	private final String message;

	public SessionError() {
		this("You do not have a valid session!");
	}

	public SessionError(String message) {
		super(ErrorKind.SessionError);
		this.message = message;
	}
}
