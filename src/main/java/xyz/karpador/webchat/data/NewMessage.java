package xyz.karpador.webchat.data;

import lombok.Data;

@Data
public class NewMessage {
	private String sessionCookie;
	private String content;
}
