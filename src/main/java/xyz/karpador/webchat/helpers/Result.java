package xyz.karpador.webchat.helpers;

import lombok.Getter;

public class Result<TResult, TErr> {
	private final @Getter boolean success;
	private final @Getter TResult value;
	private final @Getter TErr error;

	protected Result(TResult value, TErr error) {
		this.success = error == null;
		this.value = value;
		this.error = error;
	}

	public static <TResult, TErr> Result<TResult, TErr> ok(TResult result) {
		return new Result<>(result, null);
	}

	public static <TResult, TErr> Result<TResult, TErr> err(TErr error) {
		return new Result<>(null, error);
	}
}
