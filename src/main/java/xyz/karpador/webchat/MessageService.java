package xyz.karpador.webchat;

import xyz.karpador.webchat.data.Update;

public interface MessageService {
	void sendUpdate(Update update);
}
