package xyz.karpador.webchat;

import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.messaging.simp.stomp.StompHeaderAccessor;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.messaging.SessionConnectEvent;
import org.springframework.web.socket.messaging.SessionDisconnectEvent;
import org.springframework.web.socket.messaging.SessionSubscribeEvent;
import xyz.karpador.webchat.data.*;

import java.sql.Timestamp;
import java.util.Date;

@Component
@Slf4j
public class StompEventListener {

	@Autowired
	private MessageService messageService;

	@Autowired
	private UserRepository users;

	@Autowired
	private MessageRepository messages;

	@EventListener
	public void stompConnect(SessionConnectEvent event) {
		val message = event.getMessage();
		val accessor = StompHeaderAccessor.wrap(message);
		String session = accessor.getPasscode();
		users.findByActiveTrueAndSession(session)
			.ifPresent(user -> {
				log.info("Setting stompSession for User " + user.getName());
				user.setStompSession(accessor.getSessionId());
				users.save(user);
			});
	}

	@EventListener
	public void stompSubscribe(SessionSubscribeEvent event) {
		val message = event.getMessage();
		val accessor = StompHeaderAccessor.wrap(message);
		String session = accessor.getSessionId();
		users.findByActiveTrueAndStompSession(session)
			.ifPresent(user -> {
				log.info("Sending login message for " + user.getName());
				val newMessage = messages.save(
					new Message(
						user.getName() + " joined the chatroom!",
						new Timestamp(new Date().getTime()),
						"is-info", null
					)
				);
				messageService.sendUpdate(new UserJoinedUpdate(user.getName()));
				messageService.sendUpdate(new NewMessageUpdate(newMessage));
			});
	}

	@EventListener
	public void stompDisconnect(SessionDisconnectEvent event) {
		val message = event.getMessage();
		val accessor = StompHeaderAccessor.wrap(message);
		String session = accessor.getSessionId();
		users.findByActiveTrueAndStompSession(session)
			.ifPresent(user -> {
				log.info("Logging out User " + user.getName());
				user.setStompSession(null);
				users.save(user);
				val newMessage = messages.save(
					new Message(
						user.getName() + " left the chatroom!",
						new Timestamp(new Date().getTime()),
						"is-info", null
					)
				);
				messageService.sendUpdate(new UserLeftUpdate(user.getName()));
				messageService.sendUpdate(new NewMessageUpdate(newMessage));
			});
	}
}
