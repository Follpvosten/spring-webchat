package xyz.karpador.webchat;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.simp.config.ChannelRegistration;
import org.springframework.messaging.simp.config.MessageBrokerRegistry;
import org.springframework.messaging.simp.stomp.StompCommand;
import org.springframework.messaging.simp.stomp.StompHeaderAccessor;
import org.springframework.messaging.support.ChannelInterceptor;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker;
import org.springframework.web.socket.config.annotation.StompEndpointRegistry;
import org.springframework.web.socket.config.annotation.WebSocketMessageBrokerConfigurer;
import xyz.karpador.webchat.data.SessionError;
import xyz.karpador.webchat.data.UserRepository;

@Configuration
@EnableWebSocketMessageBroker
public class WebSocketConfig implements WebSocketMessageBrokerConfigurer {

	@Autowired
	private UserRepository users;

	@Autowired
	@Qualifier("clientOutboundChannel")
	private MessageChannel clientOutboundChannel;

	@Override
	public void configureMessageBroker(MessageBrokerRegistry config) {
		config.enableSimpleBroker("/queue");
	}

	@Override
	public void registerStompEndpoints(StompEndpointRegistry registry) {
		registry.addEndpoint("/message-socket").setAllowedOrigins("*");
	}

	@Override
	public void configureClientInboundChannel(ChannelRegistration registration) {
		registration.interceptors(new ChannelInterceptor() {
			@Override
			public Message<?> preSend(Message<?> message, MessageChannel channel) {
				val accessor = StompHeaderAccessor.wrap(message);
				if(StompCommand.CONNECT.equals(accessor.getCommand())) {
					String session = accessor.getPasscode();
					if(!users.findByActiveTrueAndSession(session).isPresent()) {
						val errorAccessor = StompHeaderAccessor.create(StompCommand.ERROR);
						errorAccessor.setMessage("You do not have a valid session!");
						errorAccessor.setSessionId(accessor.getSessionId());

						byte[] payload;
						try {
							payload = new ObjectMapper().writeValueAsBytes(new SessionError());
						} catch (JsonProcessingException e) {
							e.printStackTrace();
							// The try block should _never_ fail - default to a valid Error instance.
							payload = "{\"errorKind\":\"SessionError\",\"message\":\"Invalid session!\"}".getBytes();
						}

						clientOutboundChannel.send(
							MessageBuilder.createMessage(payload, errorAccessor.getMessageHeaders())
						);
						return null;
					}
				}
				return message;
			}
		});
	}


}
