package xyz.karpador.webchat;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;

import lombok.val;
import org.springframework.web.bind.annotation.*;

import xyz.karpador.webchat.data.*;
import xyz.karpador.webchat.data.Error;
import xyz.karpador.webchat.helpers.Result;

@RestController
class ChatController {

	private final UserRepository users;
	private final MessageRepository messages;
	private final MessageService messageService;

	ChatController(UserRepository users, MessageRepository messages, MessageService messageService) {
		this.users = users;
		this.messages = messages;
		this.messageService = messageService;
	}

	@PostMapping("/login/{name}")
	Result<User, Error> login(
		HttpServletResponse response,
		@PathVariable String name,
		@CookieValue(value = "session", defaultValue = "") String session) {
		if (session.isEmpty()) {
			session = UUID.randomUUID().toString();
			Cookie cookie = new Cookie("session", session);
			cookie.setMaxAge(60 * 60 * 24 * 7);
			cookie.setPath("/");
			response.addCookie(cookie);
		}
		final String sessionCookie = session;
		// Check if the name already exists
		if (name.equals("Spring Chat") || users.findByActiveTrueAndName(name).isPresent()) {
			return Result.err(new GeneralError("Name is already taken!"));
		}
		User user = users
			.findByActiveTrueAndSession(sessionCookie)
			.map(u -> {
				u.setName(name);
				u.setActive(true);
				return users.save(u);
			})
			.orElseGet(() -> users.save(new User(name, true, sessionCookie, null)));
		return Result.ok(user);
	}

	@GetMapping("/me")
	Result<User, SessionError> getMe(@CookieValue(value = "session", defaultValue = "") String session) {
		return session.isEmpty()
			? Result.err(new SessionError())
			: users.findByActiveTrueAndSession(session).map(Result::<User, SessionError>ok)
			.orElseGet(() -> Result.err(new SessionError()));
	}

	@GetMapping("/messages")
	Result<List<Message>, SessionError> allMessages(@CookieValue(value = "session", defaultValue = "") String session) {
		return session.isEmpty()
			? Result.err(new SessionError("You do not have a valid session!"))
			: Result.ok(messages.findByOrderBySentTimestamp());
	}

	@GetMapping("/users")
	Result<List<String>, SessionError> allUsers(@CookieValue(value = "session", defaultValue = "") String session) {
		return session.isEmpty()
			? Result.err(new SessionError())
			: Result.ok(
			users.findByActiveTrueAndStompSessionNotNull().parallelStream()
				.map(User::getName)
				.collect(Collectors.toList()));
	}

	@PostMapping("/message")
	Result<Long, SessionError> sendMessage(@CookieValue(value = "session", defaultValue = "") String session,
	                                       @RequestBody Message newMessage) {
		return session.isEmpty()
			? Result.err(new SessionError())
			: users.findByActiveTrueAndSession(session)
			.map(u -> {
				newMessage.setUser(u);
				newMessage.setSentTimestamp(new Timestamp(new Date().getTime()));
				val savedMsg = messages.save(newMessage);
				messageService.sendUpdate(new NewMessageUpdate(savedMsg));
				return Result.<Long, SessionError>ok(savedMsg.getId());
			})
			.orElseGet(() -> Result.err(new SessionError()));
	}

	@PatchMapping("/message/{id}")
	Result<Message, Error> editMessage(@CookieValue(value = "session", defaultValue = "") String session,
	                                   @PathVariable Long id,
	                                   @RequestBody String newContent) {
		return session.isEmpty()
			? Result.err(new SessionError())
			: users.findByActiveTrueAndSession(session)
			.map(user -> messages.findById(id)
				.map(msg -> msg.getUser()
					.map(msgUser -> {
						if (!msgUser.getId().equals(user.getId())) {
							return Result.<Message, Error>err(
								new GeneralError("You can only edit your own messages!")
							);
						}
						msg.setContent(newContent);
						msg.setEditedTimestamp(new Timestamp(new Date().getTime()));
						val savedMsg = messages.save(msg);
						messageService.sendUpdate(
							new EditedMessageUpdate(
								savedMsg.getId(), savedMsg.getContent(),
								savedMsg.getEditedTimestamp()
							));
						return Result.<Message, Error>ok(savedMsg);
					}).orElseGet(() -> Result.err(new GeneralError("You can only edit your own messages!"))))
				.orElseGet(() -> Result.err(new GeneralError("This message does not exist!"))))
			.orElseGet(() -> Result.err(new SessionError()));
	}
}
