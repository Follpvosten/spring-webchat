package xyz.karpador.webchat;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;
import xyz.karpador.webchat.data.Update;

@Service
public class MessageServiceImpl implements MessageService {

	private final SimpMessagingTemplate template;

	@Autowired
	public MessageServiceImpl(SimpMessagingTemplate template) {
		this.template = template;
	}

	@Override
	public void sendUpdate(Update update) {
		this.template.convertAndSend("/queue/updates", update);
	}
}
