/* Chat client implementation */

var me: User;
var messages: Message[];
var client: StompJs.Client;
var useNotifications = false;
var currentColor = UI.BulmaColor.Light;

$(document).ready(() => {
    initialSetup();
});

async function initialSetup() {
    try {
        const userResult = await Comm.getRequest<User>("/me");
        if (userResult.success == true) {
            me = userResult.value;
        } else {
            await UI.showMessage(userResult.error.message, "Error");
            if (userResult.error.errorKind == ErrorKind.SessionError)
                location.replace("index.html");
            return;
        }
        const messagesResult = await loadAllMessages();
        if (messagesResult.success == true) {
            messages = messagesResult.value;
        } else {
            await UI.showMessage(messagesResult.error.message, "Error");
            if (messagesResult.error.errorKind == ErrorKind.SessionError)
                location.replace("index.html");
            return;
        }
        renderAllMessages();
        const usersResult = await loadAllUsers();
        if (usersResult.success == true) {
            const $users = $("#users").empty();
            usersResult.value.forEach(u =>
                $users.append(renderUser(u))
            );
        } else {
            await UI.showMessage(usersResult.error.message, "Error");
            if (usersResult.error.errorKind == ErrorKind.SessionError)
                location.replace("index.html");
            return;
        }

        // Set up websockets
        const protocol = location.protocol == "https:" ? "wss:" : "ws:";
        client = new StompJs.Client({
            brokerURL: `${protocol}//${location.host}/message-socket`,
            connectHeaders: { passcode: Util.getCookie("session") },
            debug: str => console.log(str),
            reconnectDelay: 500,
            heartbeatIncoming: 4000,
            heartbeatOutgoing: 4000,
            onConnect: () => {
                client.subscribe("/queue/updates", updateReceived);
            },
            onStompError: async frame => {
                const message = frame.headers["message"],
                    error: ServerError = JSON.parse(frame.body);
                console.log('Broker reported error: ' + message);
                console.log('Additional details: ' + frame.body);
                if (error) {
                    client.deactivate();
                    await UI.showMessage(error.message, "Error");
                    if (error.errorKind == ErrorKind.SessionError) {
                        location.replace("index.html");
                    } else {
                        client.activate();
                    }
                }
            }
        });
        client.activate();

        // Set up message sending events
        const $sendButton = $("#send-button").click(sendMessage);
        $("#message-input").keypress(e => { if (e.keyCode == 13) $sendButton.click(); });
        $("#color-select").change(() => {
            const newColor = <UI.BulmaColor>$("#color-select").val();
            $("[data-role='sendcontrol']")
                .removeClass(currentColor)
                .addClass(newColor);
            currentColor = newColor;
            localStorage.setItem("color", currentColor);
        });
        const savedColor = localStorage.getItem("color");
        if (savedColor) {
            $(`#color-select > [value='${savedColor}']`).prop("selected", true);
            $("#color-select").change();
        }
        $("#logout-button").click(() => {
            client.deactivate();
            location.replace("index.html");
        });

        // Ask for notification permissions if we can
        if ("Notification" in window) {
            // If we have not yet asked for permissions, ask now;
            // otherwise, use the already set value
            const permission = Notification.permission === "default"
                ? await Notification.requestPermission()
                : Notification.permission;
            useNotifications = permission !== "denied";
        }
    } catch (e) {
        console.log(e);
        const error = <SpringError>e;
        UI.showMessage(`${error.status} ${error.error}`, "Network error");
    }
}

async function sendNotification(title: string, body: string) {
    if (useNotifications) {
        new Notification(title, { body: body });
    }
}

function toEdited(timeString: string) {
    if (!timeString) return "";
    return "(edited " + new Date(timeString).toLocaleTimeString() + ")";
}

function updateReceived(msg: StompJs.Message) {
    if (msg.body) {
        let update: Update = JSON.parse(msg.body);
        switch (update.updateKind) {
            case UpdateKind.NewMessage: {
                const message = update.message;
                messages.push(message);
                let $messages = $("#messages")
                    .append(renderMessage(message));
                // TODO: Only scroll down if the user hasn't scrolled up
                $messages.scrollTop($messages[0].scrollHeight);
                // Send notification if the message is not from the current user
                // and not from "Spring Chat". The latter are login/logout updates
                // that should already be handled by the UserJoined/UserLeft cases.
                if (![me.name, "Spring Chat"].includes(message.userName))
                    sendNotification(message.userName + " - Spring Chat", message.content);
                break;
            }
            case UpdateKind.EditedMessage: {
                const editedUpdate = update;
                let message = messages.find(mesg => mesg.id == editedUpdate.messageId);
                message.content = editedUpdate.newContent;
                message.editedTimestamp = editedUpdate.editedTime;
                $(`#messages > [data-id='${editedUpdate.messageId}']`).replaceWith(renderMessage(message));
                break;
            }
            case UpdateKind.UserJoined: {
                // Ignore the updated if there was no leave
                if (!$(`#users > [data-name='${update.name}']`).length) {
                    $("#users").append(renderUser(update.name));
                    if (update.name !== me.name)
                        sendNotification("Spring Chat", update.name + " joined the chatroom!");
                }
                break;
            }
            case UpdateKind.UserLeft: {
                $(`#users > [data-name='${update.name}']`).remove();
                sendNotification("Spring Chat", update.name + " left the chatroom!");
                break;
            }
            default: {
                const _neverCheck: never = update;
                console.error("Received unknown update kind: " + JSON.stringify(_neverCheck));
            }
        }
    } else {
        console.log("got empty message");
    }
}

function loadAllMessages() {
    return Comm.getRequest<Message[]>("/messages");
}
function loadAllUsers() {
    return Comm.getRequest<string[]>("/users");
}

function sendMessage() {
    const text = $("#message-input").val().toString().trim();
    if (!text) {
        UI.showMessage("Please write a message. lol.");
        return;
    }
    const message: Message = {
        content: text,
        sentTimestamp: null,
        editedTimestamp: null,
        color: currentColor
    };
    const $messageInput = $("#message-input").prop("disabled", true);
    const $sendButton = $("#send-button").prop("disabled", true).addClass("is-loading");
    Comm.postRequest<Message>("/message", JSON.stringify(message)).then(async result => {
        if (result.success == true) {
            $messageInput.val("").prop("disabled", false);
            $sendButton.prop("disabled", false).removeClass("is-loading");
        } else {
            await UI.showMessage(result.error.message, "Error");
            if (result.error.errorKind == ErrorKind.SessionError)
                location.replace("index.html");
            return;
        }
    }).catch((err: SpringError) =>
        UI.showMessage(err.status + " " + err.error, "Error")
    );
}

function renderMessage(message: Message) {
    const time = new Date(message.sentTimestamp).toLocaleTimeString();
    const edited = toEdited(message.editedTimestamp);
    const $result = $(`<div data-id="${message.id}" class="notification content ${message.color}">
  <strong>${message.userName}</strong> <small>${time}</small> <small data-field="edited">${edited}</small>
  <span><p data-field="content">${message.content}</p></span>
  <p data-role="help" class="help is-danger" style="display: none;"></p>
</div>`);
    if (message.userName === me.name) {
        $result.click(() => {
            // Activate editing mode
            const $input = $(`<input type="text" class="input">`);
            $input.val(message.content)
                .keypress(e => {
                    if (e.keyCode == 13) {
                        $input.prop("disabled", true);
                        Comm.patchRequest<Message>("/message/" + message.id, <string>$input.val())
                            .then(async result => {
                                if (result.success == false) {
                                    // Only handle errors - success will replace $result
                                    const error = result.error;
                                    if (error.errorKind == ErrorKind.SessionError) {
                                        await UI.showMessage(error.message, "Error");
                                        location.replace("index.html");
                                    } else {
                                        $input.prop("disabled", false);
                                        const $help = $result.find("[data-role='help']").text(error.message)
                                            .slideDown(400, null, () => setTimeout(() => $help.slideUp(400), 2000));
                                    }
                                }
                            })
                            .catch((err: SpringError) =>
                                UI.showMessage(err.status + " " + err.message, "Error")
                            );
                    }
                })
                .blur(() => {
                    // If the content has changed...
                    if (message.content !== $input.val()) {
                        const event = $.Event("keypress");
                        event.keyCode = 13;
                        $input.trigger(event);
                    } else {
                        // Otherwise, just re-render the message content
                        $input.replaceWith(`<p data-field="content">${message.content}</p>`);
                    }
                });
            $result.find("[data-field='content']").replaceWith($input);
            $input.focus();
        });
    }
    return $result;
}

function renderAllMessages() {
    const $messages = $("#messages").empty();
    messages
        .map(msg => renderMessage(msg))
        .forEach(msg => $messages.append(msg));
}

function renderUser(userName: string) {
    return `<p data-name="${userName}"><a>@${userName}</a></p>`;
}
