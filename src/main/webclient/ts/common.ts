/* Common functions for login.ts and chat.ts */

enum UpdateKind {
    NewMessage = "NewMessage",
    EditedMessage = "EditedMessage",
    UserJoined = "UserJoined",
    UserLeft = "UserLeft"
}

enum ErrorKind {
    SessionError = "SessionError",
    OtherError = "OtherError"
}

namespace Util {
    export function getCookie(name: string) {
        const cookie = name + "=";
        return (document.cookie.split("; ")
            .find(c => c.startsWith(cookie)) || cookie)
            .split("=")[1] || "";
    }

    export function failWith(message: string): never {
        throw new Error(message);
    }
}

namespace Comm {
    type RequestType = 'POST' | 'GET' | 'PUT' | 'PATCH';

    function sendRequest<T>(params: { uri: string, data?: any, contentType?: string, type: RequestType }): Promise<Result<T, ServerError>> {
        return new Promise((resolve, reject: (reason: SpringError) => void) => {
            $.ajax(params.uri, {
                type: params.type,
                data: params.data || {},
                dataType: 'json',
                contentType: params.contentType || 'application/json',
                xhrFields: { withCredentials: true },
                success: result => resolve(result),
                error: (xhr) => reject(xhr.responseJSON)
            });
        });
    }

    export const getRequest = <T>(uri: string, data?: any) =>
        sendRequest<T>({ uri: uri, data: data, type: 'GET' });

    export const postRequest = <T>(uri: string, data?: any) =>
        sendRequest<T>({ uri: uri, data: data, type: 'POST' });

    export const patchRequest = <T>(uri: string, data?: any) =>
        sendRequest<T>({ uri: uri, data: data, type: 'PATCH', contentType: 'text/plain' });
}

namespace UI {
    export const showMessage = (message: string, title = "Info") =>
        new Promise<void>(resolve => {
            let $modal = $(`
<div class="modal is-active">
  <div class="modal-background"></div>
  <div class="modal-card">
    <header class="modal-card-head">
      <p class="modal-card-title">${title}</p>
    </header>
    <section class="modal-card-body">
      <div class="content">${message}</div>
    </section>
    <footer class="modal-card-foot is-right">
      <button class="button ok-button">OK</button>
    </footer>
  </div>
</div>`);
            let $okButton = $modal.find(".ok-button").click(() => {
                $modal.remove();
                resolve();
            });
            $("body").append($modal);
            $okButton.focus();
        });

    export enum BulmaColor {
        Light = "is-light",
        Dark = "is-dark",
        Black = "is-black",
        Primary = "is-primary",
        Link = "is-link",
        Info = "is-info",
        Success = "is-success",
        Warning = "is-warning",
        Danger = "is-danger"
    }
}
