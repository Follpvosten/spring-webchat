import * as esm5 from '../node_modules/@stomp/stompjs/esm5/index';

declare global {
    namespace StompJs {
        export import Client = esm5.Client;
        export import Frame = esm5.Frame;
        export import Message = esm5.Message;
        export import Parser = esm5.Parser;
        export import StompConfig = esm5.StompConfig;
        export import StompHeaders = esm5.StompHeaders;
        export import StompSubscription = esm5.StompSubscription;
        export import Transaction = esm5.Transaction;
    }
}
