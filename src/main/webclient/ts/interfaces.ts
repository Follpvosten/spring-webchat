/* interfaces for Spring-Chat */

type Result<TResult, TErr> = Succeeded<TResult> | Failed<TErr>;

interface Succeeded<TResult> {
    success: true;
    value: TResult;
    error: null;
}

interface Failed<TErr> {
    success: false;
    value: null;
    error: TErr;
}

type Update = NewMessageUpdate | EditedMessageUpdate | UserUpdate;

interface NewMessageUpdate {
    updateKind: UpdateKind.NewMessage,
    message: Message
}

interface EditedMessageUpdate {
    updateKind: UpdateKind.EditedMessage,
    messageId: number,
    newContent: string,
    editedTime: string
}

interface UserUpdate {
    updateKind: UpdateKind.UserJoined | UpdateKind.UserLeft,
    name: string;
}

interface User {
    id: number;
    name: string;
    active: boolean;
}

interface Message {
    id?: number;
    content: string;
    sentTimestamp: string;
    editedTimestamp: string;
    color: UI.BulmaColor;
    userName?: string;
}

interface SpringError {
    error: string;
    message: string;
    path: string;
    status: number;
    timestamp: string;
}

interface ServerError {
    errorKind: ErrorKind;
    message: string;
}
