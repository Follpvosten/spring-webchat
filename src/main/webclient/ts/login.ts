/* Login functionalities */

$(document).ready(() => {
    let $joinButton = $("#join-button").click(tryLogin);
    $("#name-input").keypress(e => { if (e.keyCode == 13) $joinButton.click(); });
});

function login(name: string) {
    return Comm.postRequest<User>(`/login/${encodeURIComponent(name)}`);
}

function tryLogin() {
    const $nameInput = $("#name-input").prop("disabled", true);
    const name = $nameInput.val().toString().trim();
    if (!name) {
        let $nameHint = $("#name-hint")
            .addClass("is-danger")
            .text("Please provide a name!")
            .slideDown(100, () =>
                setTimeout(() => $nameHint.slideUp(100), 1000)
            );
        $nameInput.prop("disabled", false);
        return;
    }

    let $joinButton = $("#join-button").prop("disabled", true).addClass("is-loading");
    login(name).then(async result => {
        if (result.success == true) {
            localStorage.setItem("lastUser", JSON.stringify(result.value));
            location.replace("chat.html");
        } else {
            await UI.showMessage("Could not log in: " + result.error.message, "Error");
            $nameInput.prop("disabled", false).focus();
            $joinButton.prop("disabled", false).removeClass("is-loading");
        }
    }).catch((err: SpringError) => {
        console.error(err);
        UI.showMessage(err.status + " " + err.error, "Error");
    });
}
