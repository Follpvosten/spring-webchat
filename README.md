# Spring Webchat

A very simple, self-contained chat webapp written in Java with Spring at the
backend, and in TypeScript with Bulma and jQuery on the frontend.

It uses HTTP requests for setting user names and sending messages, but a
websocket and STOMP based approach for receiving them.

## Goals of this project

My main objective with this is to learn the Spring Framework.

However, since I want to learn one Spring component after another, the Spring
Security aspect was skipped for now; as such, this project can also serve as
an example for identifying users based on HTTP sessions without using Spring
Security (which might be overkill if you're just creating a small project or
want to focus on other parts of the framework).

It also shows a way to handle simple STOMP authentication based on those HTTP
sessions (not a very beautiful one, but better than nothing).

## Other details

The client-side technologies were chosen here because I'm already familiar with
them, so I don't have to think about them and can focus on learning how to use
Spring.
